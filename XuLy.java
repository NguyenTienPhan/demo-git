
/*
 * Bài toán : Đếm số lượng chữ số của số nguyên dương n
 * Người tạo : Nguyễn Tiến Phan
 * Ngày tạo : 20/02/2021
 * */
import java.util.Scanner;

public class XuLy {

	public XuLy() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n;
		do {
			System.out.println("Nhập n > 0: ");
			n = Integer.parseInt(scan.nextLine());
		} while (n < 1);
		int count = demChuSo(n);
		System.out.println("Số " + n + " có " + count + " chữ số ");

	}

	public static int demChuSo(int n) {
		if (n < 10) {
			return 1;
		} else {
			return demChuSo(n / 10) + 1;
		}
	}
}
